import { Meteor } from 'meteor/meteor'
import API from '/api/server'

Meteor.methods({
  fetchStages() {
    return API.fetchStages()
  },
  changeStages(changes) {
    try {
      return API.saveStages(changes)
    } catch (e) {
      throw new Meteor.Error('Change Stages', e)
    }
  }
})
