import { resetDatabase } from 'meteor/xolvio:cleaner'
import chai from 'chai'
import { Stages } from './stages'
import API from '/api/server'

describe('Db save', function() {
  beforeEach(function() {
    resetDatabase()
    const initialStages = [
      { _id: 'Yc3fprWMnhXtWWzfH', name: 'Stage one', number: 1 },
      { _id: 'Cyvt2eCoaoxwPCwLW', name: 'Stage two', number: 2 },
      { _id: 'G2DkccHveGk5NRgYh', name: 'Stage three', number: 3 },
      { _id: 'jLtSktZA8BFLfkftb', name: 'Stage four', number: 4 }
    ]
    initialStages.forEach(stage => Stages.insert(stage))
  })

  it('Deletes the stage', function () {
    const stage = Stages.findOne({ number: 1 })
    API.saveStages({
      added: [],
      modified: [],
      deleted: [stage]
    })
    const stages = Stages.find({}).fetch()
    const expectedStages = [
      { _id: 'Cyvt2eCoaoxwPCwLW', name: 'Stage two', number: 2 },
      { _id: 'G2DkccHveGk5NRgYh', name: 'Stage three', number: 3 },
      { _id: 'jLtSktZA8BFLfkftb', name: 'Stage four', number: 4 }
    ]
    chai.assert.deepEqual(stages, expectedStages)
  })
  it('Modifies the stage', function () {
    const stage = Stages.findOne({ number: 1 })
    stage.name = 'Stage modified'
    API.saveStages({
      added: [],
      modified: [stage],
      deleted: []
    })
    const stages = Stages.find({}).fetch()
    const expectedStages = [
      { _id: 'Yc3fprWMnhXtWWzfH', name: 'Stage modified', number: 1 },
      { _id: 'Cyvt2eCoaoxwPCwLW', name: 'Stage two', number: 2 },
      { _id: 'G2DkccHveGk5NRgYh', name: 'Stage three', number: 3 },
      { _id: 'jLtSktZA8BFLfkftb', name: 'Stage four', number: 4 }
    ]
    chai.assert.deepEqual(stages, expectedStages)
  })
  it('Adds the stage', function () {
    const stage = {
      name: 'Stage five',
      number: 5
    }
    API.saveStages({
      added: [stage],
      modified: [],
      deleted: []
    })
    const stages = Stages.find({}).fetch()
    const stagesWithoutId = stages.map(s => {
      return {
        name: s.name,
        number: s.number
      }
    })
    const expectedStages = [
      { name: 'Stage one', number: 1 },
      { name: 'Stage two', number: 2 },
      { name: 'Stage three', number: 3 },
      { name: 'Stage four', number: 4 },
      { name: 'Stage five', number: 5 }
    ]
    chai.assert.deepEqual(stagesWithoutId, expectedStages)
  })
  it('Stages added and modified', function () {
    const addedStage = { name: 'Stage five', number: 5 }
    const modifiedStages = [
      { _id: 'Yc3fprWMnhXtWWzfH', name: 'Stage three', number: 1 },
      { _id: 'Cyvt2eCoaoxwPCwLW', name: 'Stage one', number: 2 },
      { _id: 'G2DkccHveGk5NRgYh', name: 'Stage two', number: 4 },
      { _id: 'jLtSktZA8BFLfkftb', name: 'Stage four', number: 3 }
    ]
    API.saveStages({
      added: [addedStage],
      modified: modifiedStages,
      deleted: []
    })
    const stages = Stages.find({}).fetch()
    const stagesWithoutId = stages.map(s => {
      return {
        name: s.name,
        number: s.number
      }
    })
    const expectedStages = [
      { name: 'Stage three', number: 1 },
      { name: 'Stage one', number: 2 },
      { name: 'Stage two', number: 4 },
      { name: 'Stage four', number: 3 },
      { name: 'Stage five', number: 5 }
    ]
    chai.assert.deepEqual(stagesWithoutId, expectedStages)
  })
})

// stages { added: [],
//     deleted: [ { _id: '8DitMNL8ua6d3D56e', name: 'Stage five', number: 1 } ],
//     modified:
//      [ { _id: 'zcd5HrWkAHhrbi9MS', name: 'Stage five', number: 1 },
//        { _id: 'fnHZwfnC8n2HbnMzy', name: 'Stage one', number: 2 },
//        { _id: 'iZZ3e4bzouGfp6pbP', name: 'Stage two', number: 3 },
//        { _id: 'A8nFW2gDBdQZzNGBN', name: 'Stage four', number: 4 } ] }