import { Stages } from './stages'

export default {
  fetchStages,
  saveStages
}

function fetchStages() {
  return Stages.find({}, { sort: { number: 1 } }).fetch()
}

function saveStages(stages) {
  if (stages.deleted.length) {
    stages.deleted.forEach(stage => {
      Stages.remove(stage._id)
    })
  }
  if (stages.modified.length) {
    stages.modified.forEach(stage => {
      Stages.upsert(stage._id, {
        name: stage.name,
        number: stage.number
      })
    })
  }
  if (stages.added.length) {
    const ids = stages.added.map(stage => {
      return Stages.insert({
        name: stage.name || '',
        number: stage.number || ''
      })
    })
    return ids
  }
}
