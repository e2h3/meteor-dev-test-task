import { Mongo } from 'meteor/mongo'
export const Stages = new Mongo.Collection('stages')

Stages.deny({
  insert() {
    return true
  },
  update() {
    return true
  },
  remove() {
    return true
  }
})
