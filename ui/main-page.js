import { Template } from 'meteor/templating'
import API from '/api/client'
import { StageListState } from './components/lists/stage-list-state'
import './components/icons/back-arrow'
import './components/icons/edit'
import './components/lists/stage-list'
import './main-page.html'

const t = Template.mainPage
t.onCreated(function() {
  this.init = () => {
    this.listState = new StageListState()
    this.loadStages()
  }

  this.loadStages = async () => {
    try {
      const stages = await API.loadStages()
      this.listState.initWithStages(stages)
    } catch (err) {
      console.log('Failed to init stage list')
      console.error(err)
    }
  }

  this.getListState = () => this.listState

  this.init()
})

t.helpers({
  listState() {
    return Template.instance().getListState()
  }
})
