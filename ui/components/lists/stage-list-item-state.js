import { ReactiveVar } from 'meteor/reactive-var'
import { SortableListItemState } from './sortable-list-item-state'

export class StageListItemState extends SortableListItemState {
  constructor({ stage, options = {} }) {
    super({ options, itemTemplateName: 'stageListItem' })
    this.stage = stage
    this._initStageName()
    this.isNewItemForm = options.isNewItemForm || false
  }

  get stageName() {
    return this._stageName.get()
  }

  set stageName(value) {
    this._stageName.set(value.trim())
  }

  reset(position) {
    super.reset(position)
    this._initStageName()
  }

  _initStageName() {
    const initialStageName = (this.stage && this.stage.name) || ''
    if (!this._stageName) {
      this._stageName = new ReactiveVar()
    }

    this._stageName.set(initialStageName)
  }

  getCurrentData() {
    if (this.isNewItemForm) {
      return
    }

    return {
      ...this.stage,
      number: this.position,
      name: this.stageName
    }
  }

  hasChanges() {
    return this.stage && this.stageName !== this.stage.name
  }

  revertChanges() {
    this.stageName = this.stage.name
  }
}
