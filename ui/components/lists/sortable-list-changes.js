import { Mongo } from 'meteor/mongo'

export class SortableListChanges {
  constructor() {
    this.changes = new Mongo.Collection(null)
  }

  hasChanges() {
    return this.changes.find().count() > 0
  }

  getChanges() {
    const added = this._getChangedItemIds({ added: true })
    const modified = this._getChangedItemIds({ modified: true })
    const reordered = this._getChangedItemIds({ reordered: true })
    const deleted = this._getChangedItemIds({ deleted: true })
    return { added, modified, reordered, deleted }
  }

  added(itemId) {
    this.changes.insert({ itemId, added: true })
  }

  deleted(itemId) {
    const wasAddedBefore = this._itemWasAddedBefore(itemId)
    this.clear(itemId)
    if (!wasAddedBefore) {
      this.changes.insert({ itemId, deleted: true })
    }
  }

  modified(itemId) {
    const wasAddedBefore = this._itemWasAddedBefore(itemId)
    if (wasAddedBefore) {
      return
    }
    this.changes.upsert(
      {
        itemId,
        modified: true
      },
      {
        $set: { itemId, modified: true }
      }
    )
  }

  modifiedBack(itemId) {
    this.changes.remove({ itemId, modified: true })
  }

  reordered(itemId) {
    const wasAddedBefore = this._itemWasAddedBefore(itemId)
    if (wasAddedBefore) {
      return
    }

    this.changes.upsert(
      {
        itemId,
        reordered: true
      },
      {
        $set: { itemId, reordered: true }
      }
    )
  }

  reorderedBack(itemId) {
    this.changes.remove({ itemId, reordered: true })
  }

  clear(itemId) {
    this.changes.remove({ itemId })
  }

  clearAll() {
    this.changes.remove({})
  }

  _getChangedItemIds(selector) {
    return this.changes.find(selector).map(change => change.itemId)
  }

  _itemWasAddedBefore(itemId) {
    return this.changes.find({ itemId, added: true }).count() > 0
  }
}
