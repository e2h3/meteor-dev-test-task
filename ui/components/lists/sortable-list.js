import { Template } from 'meteor/templating'
import { Blaze } from 'meteor/blaze'
import Sortable from 'sortablejs'

import './sortable-list.html'

const t = Template.sortableList

t.onCreated(function() {
  this.getEventHandler = handlerName => {
    const { eventHandlers } = this.data
    if (!eventHandlers) {
      return
    }

    return eventHandlers[handlerName]
  }

  this.addItem = item => {
    this.renderItem(item)
    this.updateItemsOrder()
    this.initSortable()
  }

  this.deleteItem = item => {
    this.removeRenderedItem(item.itemId)
    this.updateItemsOrder()
  }

  this.resetList = () => {
    this.renderedItems.forEach((view, itemId) =>
      this.removeRenderedItem(itemId)
    )
    const items = this.data.list.getItems()
    this.renderItems(items)
    this.initSortable()
  }

  this.data.list.on('addItem', this.addItem)
  this.data.list.on('deleteItem', this.deleteItem)
  this.data.list.on('reset', this.resetList)
})

t.onDestroyed(function() {
  this.data.list.off('addItem', this.addItem)
  this.data.list.off('deleteItem', this.deleteItem)
  this.data.list.off('reset', this.resetList)
})

t.onRendered(function() {
  this.init = () => {
    this.renderedItems = new Map()
    this.listElement = this.$('.js-items-list').get(0)
    const listItems = this.data.list.getItems()
    this.renderItems(listItems)
    this.initSortable()
  }

  this.renderItems = items => items.forEach(item => this.renderItem(item))

  this.renderItem = listItem => {
    const position = listItem.position
    const parentNode = this.renderItemContainerNode(listItem, position)
    const { eventHandlers, list, ...extraContext } = this.data
    const dataContext = { ...extraContext, item: listItem, list }
    const view = Blaze.renderWithData(
      Template[listItem.templateName],
      dataContext,
      parentNode
    )
    this.renderedItems.set(listItem.itemId, view)
  }

  this.renderItemContainerNode = (item, position) => {
    const container = document.createElement('div')
    container.className = item.sortable ? 'item js-sortable' : 'item'
    container.setAttribute('data-itemId', item.itemId)
    if (position > 0) {
      const currentItemAtPosition = this.getItemsElements()[position - 1]
      if (currentItemAtPosition) {
        this.$(currentItemAtPosition).before(container)
      } else {
        this.$(this.listElement).append(container)
      }
    } else {
      this.$(this.listElement).append(container)
    }
    return container
  }

  this.initSortable = () => {
    if (this.sortable) {
      this.sortable.destroy()
    }
    const sortableOptions = this.composeSortableOptions()
    this.sortable = new Sortable(this.listElement, sortableOptions)
  }

  this.composeSortableOptions = () => {
    const uiOptions = this.data.uiOptions || {}
    return {
      draggable: '.item.js-sortable',
      animation: 250,
      onEnd: () => this.updateItemsOrder(),
      onMove: event => this.handleItemMoveEvent(event),
      ...uiOptions
    }
  }

  this.handleItemMoveEvent = event => {
    const handler = this.getEventHandler('onMove')
    if (!handler) {
      return
    }

    const draggedItemId = $(event.dragged)
      .data('itemid')
      .toString()
    const relatedItemId = $(event.related)
      .data('itemid')
      .toString()
    return handler({ draggedItemId, relatedItemId, event })
  }

  this.removeRenderedItem = itemId => {
    const view = this.renderedItems.get(itemId)
    const container = view.firstNode().parentNode
    Blaze.remove(view)
    this.$(container).remove()
    this.renderedItems.delete(itemId)
  }

  this.updateItemsOrder = () => {
    const itemsOrder = {}
    const displayedItems = this.getItemsElements()
    displayedItems.each((index, element) => {
      const itemId = $(element).data('itemid')
      itemsOrder[itemId] = index + 1
    })
    this.data.list.updateItemsPosition(itemsOrder)
  }

  this.getItemsElements = () =>
    this.$('.js-items-list')
      .first()
      .children('.item')

  this.init()
})
