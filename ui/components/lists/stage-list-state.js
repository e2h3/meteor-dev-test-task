import { SortableListState } from './sortable-list-state'
import { StageListItemState } from './stage-list-item-state'

export class StageListState extends SortableListState {
  initWithStages(stages) {
    const items = stages.map(stage => new StageListItemState({ stage }))
    items.push(
      new StageListItemState({
        options: { sortable: false, isNewItemForm: true }
      })
    )
    this.init(items)
  }

  getChanges() {
    const itemsChanges = super.getChanges()
    return {
      added: this._composeAddedChanges(itemsChanges.added),
      deleted: this._composeDeletedChanges(itemsChanges.deleted),
      modified: this._composeModifiedChanges({
        reordered: itemsChanges.reordered,
        modified: itemsChanges.modified
      })
    }
  }

  getItemsData(items) {
    return super.getItemsData(items).filter(data => Boolean(data))
  }

  _composeAddedChanges(addedItems) {
    this._lastAddedItems = addedItems
    return this.getItemsData(addedItems)
  }

  _composeDeletedChanges(deletedItems) {
    return this.getItemsData(deletedItems)
  }

  _composeModifiedChanges({ reordered = [], modified = [] }) {
    const uniqueItems = []
    const ids = new Set()
    const addToUniqueItems = item => {
      if (ids.has(item.itemId)) {
        return
      }

      ids.add(item.itemId)
      uniqueItems.push(item)
    }

    reordered.forEach(addToUniqueItems)
    modified.forEach(addToUniqueItems)

    return this.getItemsData(uniqueItems)
  }
}
