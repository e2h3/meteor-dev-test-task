import { Template } from 'meteor/templating'
import './form-input.html'

const t = Template.formInput

t.helpers({
  inputType() {
    return this.type || 'text'
  }
})

t.events({
  'change .js-input'(evt) {
    const value = evt.target.value
    const { onChange, name: inputName } = this
    onChange(inputName, value)
  },
  'input .js-input'(evt) {
    const value = evt.target.value
    const { onChange, name: inputName } = this
    onChange(inputName, value)
  }
})
