export class EventEmitter {
  constructor() {
    this._eventListeners = new Map()
  }

  addListener(eventName, listener) {
    if (!this._eventListeners.has(eventName)) {
      this._eventListeners.set(eventName, [])
    }
    this._eventListeners.get(eventName).push(listener)
  }

  removeListener(eventName, listener) {
    const listeners = this._eventListeners.get(eventName)
    if (!listener || !listeners.length) {
      return false
    }
    const index = listeners.indexOf(listener)
    if (index < 0) {
      return false
    }
    listeners.splice(index, 1)
    this._eventListeners.set(eventName, listeners)
  }

  emit(eventName, ...args) {
    const listeners = this._eventListeners.get(eventName)
    if (!listeners || !listeners.length) {
      return false
    }

    listeners.forEach(listener => {
      try {
        listener(...args)
      } catch (err) {
        console.error(err)
      }
    })
    return true
  }

  clear() {
    this._eventListeners.clear()
  }
}
